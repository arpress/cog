//
//  SideWindowController.h
//  Cog
//
//  Created by Vincent Spader on 6/21/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "SideViewController.h"

@interface SideWindowController : SideViewController {
	NSMutableDictionary *windows;
}

@end
