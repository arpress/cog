//
//  PlaylistLoader.h
//  Cog
//
//  Created by Vincent Spader on 3/05/07.
//  Copyright 2007 Vincent Spader All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PlaylistController.h"

@class PlaylistController;
@class PlaybackController;
@class PlaylistEntry;

typedef enum {
	kPlaylistM3u,
	kPlaylistPls,
} PlaylistType;

@interface PlaylistLoader : NSObject {
	IBOutlet PlaylistController *playlistController;
}

- (void)initDefaults;

//load arrays of urls...
- (NSArray*)addURLs:(NSArray *)urls sort:(BOOL)sort;
- (NSArray*)addURL:(NSURL *)url;
- (NSArray*)insertURLs:(NSArray *)urls atIndex:(int)index sort:(BOOL)sort;

//save playlist, auto-determines type based on extension. Uses m3u if it cannot be determined.
- (BOOL)save:(NSString *)filename;
- (BOOL)save:(NSString *)filename asType:(PlaylistType)type;
- (BOOL)saveM3u:(NSString *)filename;
- (BOOL)savePls:(NSString *)filename;

//read info for a playlist entry
- (NSDictionary *)readEntryInfo:(PlaylistEntry *)pe;

- (void)loadInfoForEntries:(NSArray *)entries;

- (NSArray *)acceptableFileTypes;
- (NSArray *)acceptablePlaylistTypes; //Only m3u and pls saving
- (NSArray *)acceptableContainerTypes;

// Event inlets (passed to playlist controler):
- (void)willInsertFiles:(NSArray*)urls origin:(AddedFilesSource)src;
- (void)didInsertFiles:(NSArray*)entries origin:(AddedFilesSource)src;

@end
